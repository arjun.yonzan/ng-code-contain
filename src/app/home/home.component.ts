import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public codes:any = []
  constructor(private http: HttpClient) {}
  ngOnInit() {
		this.http.get(`${environment.apiURL}/codes/all`).subscribe((res)=>{
  		this.codes = res;
  	},(err)=>{
  		console.log(err, "err occured.. $$$$");
  	})
  }

}
