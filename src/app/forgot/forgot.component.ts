import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {

	email:any =""
	
	showMessage:any=false

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  recover(){
  	if(this.email==""){
  		alert("email is required")
  		return
  	}

  	this.http.post('http://localhost:3000/recover', {email:this.email}).subscribe((res)=>{
  		this.email = ""
  		this.showMessage=true
  	},(err)=>{
  		console.log(err, "err occured.. $$$$");
  		// alert("Email does not exists.")
  	})
  }

}
