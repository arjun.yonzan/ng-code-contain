import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../services/DataService'
import { HttpClient } from '@angular/common/http';

declare const ace: any
declare const $: any

@Component({
	selector: 'app-code-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Input('mode') formMode: string
  ace = false
  folders: any = []
  code: any = {
    title: "",
    contents: [
      {
        label: "",
        lang: "",
        body: ""
      }
    ],
    folder: ""
  }
  showFoldersDropDown = false
  selectedFolder:any = {}
  selectedFolderId
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private http: HttpClient) { }

  ngOnInit() {
    if (this.formMode == "EDIT") {
      this.setupCode(); 
    }

    this.dataService.onGetData.subscribe((folders) => {
      this.folders = folders
      this.newCodeFolder()      
    })

    this.folders = this.dataService.folders
    if(this.folders.length){
      this.newCodeFolder()
    }

    this.dataService.codeFolderSelected.subscribe((selectedFolder)=>{
      this.selectedFolder = selectedFolder
      this.showFoldersDropDown=false
      // this.code.folder = selectedFolder._id;
    })

  }


  newCodeFolder(){
    if(this.formMode=='NEW'){
      this.route.queryParams.subscribe((param) => {
        this.selectedFolder = this.findFolder(this.folders, param.folder)
      })
    }
  }

  setupCode(){
    this.route.params.subscribe((params) => {
        this.http.get('http://localhost:3000/codes/' + params.id).subscribe((res) => {
          this.code = res

          //selectedFolder
          this.selectedFolder = this.findFolder(this.folders, this.code.folder)

          //this will apply the ace plugin as view changes..
          this.ace = false
        }, (err) => {
          console.log(err, "err occured.. $$$$");
        })
      })
  }

  ngAfterViewChecked() {
    this.applyAce()
  }

  ngAfterViewInit() {
    let _this = this;
    $('#select-folder').select2({}).on('change', function(e) {
      _this.code.folder = e.target.value
    });
  }

  applyAce() {
    if (this.code.contents) {
      if (!this.ace) {
        console.log("applying the plugin...", "$$$$");
        this.code.contents.forEach((content, index) => {
          var editor = ace.edit("code-body-" + index);
          editor.setTheme("ace/theme/monokai");
          editor.getSession().setUseWrapMode(true);
          editor.setShowPrintMargin(false);
          editor.setOptions({
            maxLines: "Infinity" //or number
          });
          editor.setFontSize(14);

          editor.on('change', function(e) {
            content.body = editor.getValue();
          });

          //SELECT 2
          $('#select-' + index).select2({}).on('change', function(e) {
            var value = e.target.value
            content.lang = value;
            editor.getSession().setMode("ace/mode/" + value);
          });

          //for the edit mode..
          if(this.formMode==="EDIT"){
            editor.setValue(content.body,1)
            if (content.lang) {
              editor.session.setMode("ace/mode/" + content.lang);
            }
          }

          //finally
          if (index + 1 == this.code.contents.length) {
            this.ace = true;
          }
        });//end foreach
      }
    }//end if
  }

  removeCodeContent(index) {
    if (confirm("Are you sure?")) {
      this.code.contents.splice(index, 1)
    }
  }

  addContentField() {
    this.code.contents.push({
      label: "",
      lang: "",
      body: ""
    })
    this.ace = false; /*setting this to false -- contentviewchecked will apply the plugin as needed*/
  }

  submit() {
    const session = JSON.parse(localStorage.getItem('plasticSession'));
    const options = {
      headers: {
        Authorization: session.token
      }
    };


    this.code.folder = this.selectedFolder._id || ""

    if (this.formMode == "NEW") {
      this.http.post('http://localhost:3000/codes', this.code, options).subscribe((res:any) => {
        console.log(res, "$$$$")
        //reset the code..
        this.code= {}
        this.router.navigate([`/arjun/codes/${res._id}/edit`])
        alert("code created successfully")
      }, (err) => {
        console.log(err, "err occured.. $$$$");
        alert("error creating code...")
      })

    }//end if

    if(this.formMode==="EDIT"){
      this.http.put('http://localhost:3000/codes/'+this.code._id, this.code, options).subscribe(res=>{
        alert("code updated succesfully");
      },err=>{
        alert("error updating code...")
      })
    }

  }

  openFoldersDropDown(){
    if(this.showFoldersDropDown){
     this.showFoldersDropDown=false
   }else{
     this.showFoldersDropDown=true
   }
  }

  selectRootFolder(){
    this.selectedFolder={}
    this.code.folder = ""
    this.showFoldersDropDown = false
  }


  findFolder(folders, selectedFolderId){
    //very important -- learned this recurssive loop..
    //this is recursive loop search with in array structure like tree....
    for(let i =0; i<folders.length; i++){
        let folder = folders[i];

        if(selectedFolderId==folder._id){
            folder.index = i //custom logic
            return folder //return
        }

        //if the folder has children call itself and check...
        if(folder.hasOwnProperty('folders')){
          let innerFolder = this.findFolder(folder.folders, selectedFolderId)
          if(innerFolder){
            return innerFolder //return...
          }
      }
    }
  }//end findFolder()

}
