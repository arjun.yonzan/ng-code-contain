import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormGroup, FormControl, Validator, NgForm} from '@angular/forms'
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {
  form:any
	password:any = {}
  constructor(private http: HttpClient, private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.queryParams.subscribe((params)=>{
      this.password.recoverCode = params.recoverCode
    })//end subscribe()
  }

  resetPassword(){
    this.http.post("http://localhost:3000/reset_password",this.password).subscribe((res)=>{
      alert("Password has been reset. Please login with the new password")
      this.password = {}
    },(err)=>{
      let error = err.error
      alert(error.error);
    })
  }

}
