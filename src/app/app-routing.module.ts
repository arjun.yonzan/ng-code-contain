import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import {CodeComponent} from './codes/code/code.component'
import {NewComponent} from './codes/new/new.component'
import { EditComponent } from './codes/edit/edit.component';
import {LoginComponent} from './login/login.component'
import {DummyComponent} from './dummy/dummy.component'
import {ProfileComponent} from './profile/profile.component'
import {RegisterComponent} from './register/register.component'
import { MainComponent } from './main/main.component';
import { IndexComponent } from './main/index/index.component';
import {ForgotComponent} from './forgot/forgot.component'
import {ResetComponent} from './reset/reset.component'

import {AdminIndexComponent} from './admin/index/index.component'
import { AdminUsersComponent } from './admin/users/users.component'


const routes: Routes = [
	{ path: '', component: HomeComponent},
	{ path: 'login', component: LoginComponent},
	{ path: 'dummy', component: DummyComponent},
	{ path: 'user/profile', component: ProfileComponent},
	{ path: 'register', component: RegisterComponent},
	{ path: 'forgot-password', component: ForgotComponent},
	{ path: 'reset', component: ResetComponent},
	{ path: 'admin', component: AdminIndexComponent},
	{ path: 'admin/users', component: AdminUsersComponent},

	
	//user paths
	{ path: ':user', component: MainComponent, children:[
		{ path: '', component: IndexComponent},
		{ path: 'folders/:folderId', component: IndexComponent},
		{ path: 'codes/new', component: NewComponent},
		{ path: 'codes/:id', component: CodeComponent},
		{ path: 'codes/:id/edit', component: EditComponent}
	]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
