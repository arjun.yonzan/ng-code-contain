import { Component, OnInit } from '@angular/core';
  	import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	public register:any = {}
  constructor(private http: HttpClient) { }

  ngOnInit() {}

  signup(){

  	//validations...
  	//check for spaces...
  	if (/\s/.test(this.register.username)) {
  		alert("no space allowed in the username")
  		return;
		}

  	if(this.register.password !== this.register.confirm_password){
  		alert("passwords doesnot match")
  		return;
  	}

  	//email validations... //not angular way update later...
  	var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!regex.test(String(this.register.email).toLowerCase())){
    	alert("invalid email");
    	return
    }

		this.http.post('http://localhost:3000/register',this.register).subscribe((res)=>{
			// user registered successfully
			alert("user registered succesfully")
			this.register = {};

  	},(err)=>{
  		console.log(err, "err occured.. $$$$");
  	})





  }

}
