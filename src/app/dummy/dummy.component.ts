import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment'
import {FolderComponent} from '../folder/folder.component'

@Component({
  selector: 'app-dummy',
  templateUrl: './dummy.component.html',
  styleUrls: ['./dummy.component.css']
})
export class DummyComponent implements OnInit {

  private folders:any = [];
  private codes:any = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getFolders()
    this.getCodes()
  }//end ngOnInit

  getCodes(){
    this.http.get(environment.apiURL+"/codes?userId=577936740a62442d570f63a3").subscribe((res:any)=>{
      this.codes=res.codes;
      console.log(this.codes, "$$$$");
    },(err)=>{
      console.log(err, "err occured.. $$$$");
    })
  }

  getFolders(){
    //getting the user folders
    this.http.get(environment.apiURL+'/folders?userId=577936740a62442d570f63a3').subscribe((res:any)=>{
      this.folders = res;
      //first loop
      this.folders.forEach((folder,index)=>{ 
        this.getChildrens(folder)
      });
    },(err)=>{
      console.log(err, "err occured.. $$$$");
    })//end fn get
  }

  //***recursive function*****
  getChildrens(folder){
    //find the children
    this.folders.forEach((folderInner, indexInner)=>{
      if(folderInner.parent == folder._id){
        if(!folder.hasOwnProperty('folders')){
          folder.folders = [folderInner]
        }else{
          folder.folders.push(folderInner)
        }
        //recursive loop
        this.getChildrens(folderInner)
      }
    });
  }//end fn getchildren
}
