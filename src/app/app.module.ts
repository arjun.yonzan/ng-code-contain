import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CodeComponent } from './codes/code/code.component';
import { LoginComponent } from './login/login.component';
import { NewComponent } from './codes/new/new.component';
import { FormComponent } from './codes/form/form.component';
import { EditComponent } from './codes/edit/edit.component';
import { DummyComponent } from './dummy/dummy.component';
import { FolderComponent } from './folder/folder.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { MainComponent } from './main/main.component';
import { IndexComponent } from './main/index/index.component';

import {DataService} from './services/DataService';
import { ForgotComponent } from './forgot/forgot.component';
import { ResetComponent } from './reset/reset.component';
import { AdminUsersComponent } from './admin/users/users.component'

import {AdminIndexComponent} from './admin/index/index.component'

import {FolderModalComponent} from './modals/folder/folder.component'
import { ModalFoldersComponent } from './modals/modal-folders/modal-folders.component';
import { CodeFoldersComponent } from './codes/form/folders/folders.component'


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CodeComponent,
    LoginComponent,
    NewComponent,
    FormComponent,
    EditComponent,
    DummyComponent,
    FolderComponent,
    ProfileComponent,
    RegisterComponent,
    MainComponent,
    IndexComponent,
    ForgotComponent,
    ResetComponent,
    AdminUsersComponent,
    AdminIndexComponent,
    FolderModalComponent,
    ModalFoldersComponent,
    CodeFoldersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
