import { Component, OnInit, Input } from '@angular/core';
import {DataService} from '../../services/DataService' //data service for communication between parent...


@Component({
  selector: 'app-modal-folders', //component name..
  templateUrl: './modal-folders.component.html',
  styleUrls: ['./modal-folders.component.css']
})
export class ModalFoldersComponent implements OnInit {
  @Input() folders:any
  @Input() selectedFolder:any
  @Input() modalSelectedFolder:any
  constructor(private dataService: DataService){}
  setFolder(folder,index){
    //select folder.. //event emit..
    // console.log({folder,index}, "0000$$$$");
    folder.index=index
    this.dataService.onModalFolderSelected.emit(folder)
  }

  ngOnInit() {
  }

}
