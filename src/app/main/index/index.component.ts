import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';

import {environment} from '../../../environments/environment'
import {FolderComponent} from '../../folder/folder.component'

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  private folders:any = [];
  private codes:any = [];

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    let userId = "577936740a62442d570f63a3"
    this.route.params.subscribe((params)=>{
       this.getCodes(userId, params.folderId);
       return;
    })
  }//end ngOnInit

  getCodes(userId, folderId){
    let url = "http://localhost:3000/codes?userId="+userId;
    if(folderId){url+="&folderId="+folderId}
    this.http.get(url).subscribe((res:any)=>{
      this.codes=res.codes;
    },(err)=>{
      console.log(err, "err occured.. $$$$");
    })
  }

  //***recursive function*****
  getChildrens(folder){
    //find the children
    this.folders.forEach((folderInner, indexInner)=>{
      if(folderInner.parent == folder._id){
        if(!folder.hasOwnProperty('folders')){
          folder.folders = [folderInner]
        }else{
          folder.folders.push(folderInner)
        }
        //recursive loop
        this.getChildrens(folderInner)
      }
    });
  }//end fn getchildren


}
