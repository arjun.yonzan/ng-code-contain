import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../../services/DataService' //data service common...
declare let $: any

@Component({
  selector: 'app-folder-modal',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.css']
})
export class FolderModalComponent implements OnInit {
  @Input() folders //get folders from the paretn..
  @Input() mode //get mode from the parent..
  @Input() selectedFolder
  private folder: any = {}
  private modalSelectedFolder:any
  private prevParentFolder:any

  constructor(private http: HttpClient, private dataService: DataService) {

    this.dataService.onModalFolderSelected.subscribe((folder) => {

      if (this.modalSelectedFolder == folder) {
        this.modalSelectedFolder = null
      } else {
        this.modalSelectedFolder = folder
      }
    })
  }

  ngOnInit() {

    this.dataService.onModalEditOpen.subscribe((selectedFolder) => {

      if(this.mode=="NEW"){
        this.modalSelectedFolder = this.folder = selectedFolder
      }else{//edit
        this.folder = selectedFolder
        this.modalSelectedFolder = this.prevParentFolder = this.findFolder(this.folders, selectedFolder.parent)
      }
      $('#folderModal').modal('show'); 
    })

  }

  findFolder(folders, selectedFolderId){
    for(let i =0; i<folders.length; i++){
        let folder = folders[i];

        if(selectedFolderId==folder._id){
            folder.index = i //custom logic
            return folder //return
        }

        //if the folder has children call itself and check...
        if(folder.hasOwnProperty('folders')){
          let innerFolder = this.findFolder(folder.folders, selectedFolderId)
          if(innerFolder){
            return innerFolder //return...
          }
      }
    }
  }//end find Folder

  submit() {
    const session = JSON.parse(localStorage.getItem('plasticSession'));
    const options = {
      headers: {
        Authorization: session.token
      }
    };
  
    //rest api call..
    if (this.mode == "NEW") {
      let newFolder = this.folder;
      if (this.modalSelectedFolder) {
         newFolder.parent = this.modalSelectedFolder._id
      }

      this.http.post('http://localhost:3000/folders', newFolder, options).subscribe((res) => {
        newFolder = res
        //folder operations..
        if (this.modalSelectedFolder) {
          if (!this.modalSelectedFolder.hasOwnProperty('folders')) {

          } else {
            this.modalSelectedFolder.folders.push(newFolder)
          }
          this.modalSelectedFolder=newFolder
        } else {
          this.folders.push(newFolder)
        }
        this.folder = {}
        // $('#folderModal').modal('hide'); //hide the modal @ success...
      }, (err) => {
        alert("error")
        console.log(err, "err occured.. $$$$");
      })
    }else{
      let updateFolder = this.folder;

      if (this.modalSelectedFolder) {
         updateFolder.parent = this.modalSelectedFolder._id
      }else{
        updateFolder.parent=""
      }
      this.http.put(`http://localhost:3000/folders/${updateFolder._id}`, updateFolder, options).subscribe((res)=>{
        //move it

        let parentFolder = this.findFolder(this.folders,updateFolder.parent)
        //push....
         if(parentFolder){
           if(parentFolder.hasOwnProperty('folders')){
            parentFolder.folders.push(updateFolder)
          }else{
            parentFolder.folders = [updateFolder]
          }
        }else{
          this.folders.push(updateFolder)
        }

        //pull...
        if(this.prevParentFolder){
          this.prevParentFolder.folders.splice(updateFolder.index,1)
          this.prevParentFolder=parentFolder
        }else{
          this.folders.splice(updateFolder.index,1)
        }         

        $('#folderModal').modal('hide'); 
        
      },(err)=>{
        
        console.log(err, "$$$$");
      })
    }//end if
  }//end submit()

  selectRootFolder(){
    // this.selectedFolder=null
  }//end selectRootFolder
}