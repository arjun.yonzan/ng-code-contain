import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFoldersComponent } from './modal-folders.component';

describe('ModalFoldersComponent', () => {
  let component: ModalFoldersComponent;
  let fixture: ComponentFixture<ModalFoldersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFoldersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFoldersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
