import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	credentials:any = {}
  constructor(private http:HttpClient, private router: Router) { }
  ngOnInit() {
  }
  login(){
		this.http.post('http://localhost:3000/login',this.credentials).subscribe((res)=>{
      localStorage.setItem('plasticSession', JSON.stringify(res));
			this.router.navigate(['arjun'],{queryParams:{haha:"haha"}})
  	},(err)=>{
      alert("error");
  		console.log(err, "err occured.. $$$$");
  	})
  }
}