import { Component, OnInit,Input } from '@angular/core';
import {DataService} from '../services/DataService' //data service for communication between parent...

@Component({
  selector: 'app-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.css']
})
export class FolderComponent implements OnInit {
	@Input() folders
  constructor(private dataService:DataService) {}
  ngOnInit() {}
  selectFolder(folder,index){
    folder.index=index
    this.dataService.onMainFolderSelected.emit(folder)
  }
  showFolderMenu(e){    
    this.dataService.onShowFolderMenu.emit(e)
  }
}
