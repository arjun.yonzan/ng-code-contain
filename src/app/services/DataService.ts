import {Injectable, EventEmitter, Output} from "@angular/core";    

@Injectable()
export class DataService {
	folders = []

	onGetData = new EventEmitter()

	onModalFolderSelected = new EventEmitter()

	onMainFolderSelected = new EventEmitter()

	onShowFolderMenu = new EventEmitter()

	onDeleteFolder  = new EventEmitter()

	onModalEditOpen = new EventEmitter()
	
	codeFolderSelected = new EventEmitter()

}