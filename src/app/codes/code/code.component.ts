import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
//Router imported...
declare let $: any
declare let ace: any

@Component({
  selector: 'app-code',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.css']
})
export class CodeComponent implements OnInit {

	code: any = {}
  ace=false;

  constructor(private http: HttpClient, 
    private route: ActivatedRoute,
    private router: Router) {
      //router injected..

    }

  ngOnInit() {

    this.route.params.subscribe((params) => {
      this.http.get(`http://localhost:3000/codes/${params.id}`).subscribe((res) => {
        this.code = res
      }, (err) => {
        console.log(err, "err occured.. $$$$");
      })
    })
  }
  ngAfterViewChecked() {
      //applying ace plugin only once.. 
      //afterviewchecked keeps running...
      if (this.code.contents) {
        if(true){
          this.code.contents.forEach((content, index)=>{
          var editor = ace.edit("code-body-"+index);
          editor.setTheme("ace/theme/monokai");
          editor.getSession().setUseWrapMode(true);
          editor.setShowPrintMargin(false);
          editor.setOptions({
            maxLines: "Infinity" //or number
          });
          editor.setFontSize(14);
          editor.setReadOnly(true)
          if(content.lang){
            editor.session.setMode("ace/mode/"+content.lang);
          }
          if(index+1 == this.code.contents.length){
            this.ace = true;
          }
        });//end foreach
        }
      }//end if
  }

  //angular...
  deleteCode(codeId){

    const session = JSON.parse(localStorage.getItem('plasticSession'));
    const options = {
      headers: {
        Authorization: session.token
      }
    };

    //delete api called..
    this.http.delete("http://localhost:3000/codes/"+codeId, options).subscribe((res)=>{
      console.log("code deleted successfully...", "$$$$");
      alert("deleted")
      //navigate to main area.. on success.
      this.router.navigate(["/arjun"])
    },(err)=>{
      console.log("error deleting code..", "$$$$");
    })
  }
}
