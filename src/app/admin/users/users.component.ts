import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class AdminUsersComponent implements OnInit {

	users:any = []

  constructor(private http: HttpClient) { }

  ngOnInit() {
		this.http.get('http://localhost:3000/users').subscribe((res)=>{

			this.users = res

  	},(err)=>{
  		console.log(err, "err occured.. $$$$");
  	})


  }

}
