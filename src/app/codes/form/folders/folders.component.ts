import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../../services/DataService'

@Component({
  selector: 'app-code-folders',
  templateUrl: './folders.component.html',
  styleUrls: ['./folders.component.css']
})
export class CodeFoldersComponent implements OnInit {
	@Input() folders:any
  @Input() selectedFolder 
	@Input() selectedFolderId
  constructor(private dataService: DataService) { }

  ngOnInit() {}

  selectCodeFolder(folder){
  	this.dataService.codeFolderSelected.emit(folder)
  }

}