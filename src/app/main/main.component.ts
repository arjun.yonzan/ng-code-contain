import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import {environment} from '../../environments/environment'
import {FolderComponent} from '../folder/folder.component'

import {DataService} from '../services/DataService'

declare const $:any

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  private folders:any = []
  private allFolders:any = []
  private codes:any = []
  private selectedFolder:any
  private modalMode:any

  constructor(private http: HttpClient, 
    private DataService:DataService,
    private router: Router) { 
     $(document).on('click',(e)=>{
        if(! ($(e.target).is('.folder-menus') || $(e.target).is('.folder-menus i'))){ 
          $('#cc-folder-options').hide()
        }
     })

    this.DataService.onMainFolderSelected.subscribe((folder)=>{
      let folderMenu = document.getElementById('cc-folder-options');
      if(this.selectedFolder){
        this.selectedFolder.active = false;
        this.selectedFolder = folder
        this.selectedFolder.active = true
      }else{
        this.selectedFolder=folder
        this.selectedFolder.active = true
      }
    })

    this.DataService.onShowFolderMenu.subscribe((e)=>{
      let target = $(e.target)
      let top = target.offset().top
      let left = target.offset().left
      $('#cc-folder-options').css({
        top: top,
        left:left
      }).show()
    })

  }// end constructor

  ngOnInit() {
    this.getFolders()
  }//end ngOnInit

  getFolders(){
    //getting the user folders
     let url = environment.apiURL
    url = 'http://localhost:3000'
    this.http.get(url+'/folders?userId=577936740a62442d570f63a3').subscribe((res:any)=>{

      //for later comparing and generating the tree
      this.allFolders = res;

      //Collecting the level 1 folders
      this.folders = res.filter((folder)=>{
        if(folder.level==1 || !folder.hasOwnProperty('parent') || folder.parent == "0" || folder.parent==""){
          return folder
        }
      })

      //**emit and share the folders data
      this.DataService.onGetData.emit(this.folders);
      this.DataService.folders = this.folders;

      //**first loop //recursive function to generate the folders
      this.folders.forEach((folder,index)=>{ 
        this.getChildrens(folder)
      });
    },(err)=>{
      console.log(err.error, "err occured.. $$$$");
    })//end fn get
  }//end fn get folders

  //***recursive function*****
  getChildrens(folder){
    //find the children
    this.allFolders.forEach((folderInner, indexInner)=>{
      if(folderInner.parent == folder._id){
        if(!folder.hasOwnProperty('folders')){
          folder.folders = [folderInner]
        }else{
          folder.folders.push(folderInner)
        }
        //recursive loop
        this.getChildrens(folderInner)
      }
    });
  }//end fn getchildren

  deleteFolder() {

    if (window.confirm("DO YOU WANT TO REMOVE THIS FOLDER?")) {
      //find folder 
      let folder = this.findFolder(this.folders, this.selectedFolder._id)
      const session = JSON.parse(localStorage.getItem('plasticSession'));
      const options = {
        headers: {
          Authorization: session.token
        }
      };
      //http call for folder delete
      //call api..

      this.http.delete('http://localhost:3000/folders/' + folder._id, options).subscribe((res: any) => {
        if (res.ok) { //if ok.
          if (folder.parent == "0" || !folder.parent) {
            //if no parent.. splice.. 
            this.folders.splice(folder.index, 1)
          } else {
            //if parent.. search parent .. folder.parent..
            let parentFolder = this.findFolder(this.folders, folder.parent)
            parentFolder.folders.splice(folder.index, 1) //remove from array
          }
        }//end if
      }, (err) => {
        console.log(err.error.error, "$$$$");
        alert("error: " + err.error.error);
      })//end delete()

    }//end if

  }//deleteFolder()

  findFolder(folders, selectedFolderId){
    //very important -- learned this recurssive loop..
    //this is recursive loop search with in array structure like tree....
    for(let i =0; i<folders.length; i++){
        let folder = folders[i];

        if(selectedFolderId==folder._id){
            folder.index = i //custom logic
            return folder //return
        }

        //if the folder has children call itself and check...
        if(folder.hasOwnProperty('folders')){
          let innerFolder = this.findFolder(folder.folders, selectedFolderId)
          if(innerFolder){
            return innerFolder //return...
          }
      }
    }
  }//end findFolder()

  editFolder(){
    this.modalMode = "EDIT"
    this.DataService.onModalEditOpen.emit(this.selectedFolder)
    // console.log("editing..", "$$$$");
    // console.log(this.selectedFolder, "$$$$");

  }
  newFolder(){
    this.modalMode="NEW"
    this.DataService.onModalEditOpen.emit(this.selectedFolder)
  }

  newCode(){
    this.router.navigate(['/arjun/codes/new'], {
      queryParams:{
        folder: this.selectedFolder._id
      }
    })
  }
}
